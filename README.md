##### Cognitev-Task ######

##### 00- Nginx server Installation #####
You will find that part in 00-Install-Nginx-Cognitev-Task.conf

##### 01- Nginx Configuration #####
You will find that part in 01-Configure-Nginx-Cognitev-Task.conf

##### 02- Nginx caching and compression #####
You will find that part in 02-Caching-and-compressing-Nginx-Cognitev-Task.conf

##### 03- Caching and Compression checking #####
You will find that part in 03-Check-Caching-and-compressing-Nginx-Cognitev-Task.conf

##### 04- Install ELK stack #####
You will find that part in 04-Install-ELK-Stack-Cognitev-Task.conf

##### 05- Install and Configure Filebeat on the Ubuntu Client #####
You will find that part in 05-Install-Filebeat-On-Ubuntu-Client-Cognitev-Task.conf

##### 06- Using Kibana to visualize the logs #####
You will find that part in 06-Install-Filebeat-On-Ubuntu-Client-Cognitev-Task.conf

##### 07- Screenshoots Folder #####
You will find that part in a seperate folder that contains 11 picture for the data visualization

##### 08- Docker compose file for automation #####
You will find that part in ((( 08-docker-compose/read-my-documentaion-for-docker-compose ))) and in ((( 08-docker-compose/docker-compose-files )))
