# Make sure that Java is installed 
java -version

# Import the public GPG key
rpm --import https://artifacts.elastic.co/GPG-KEY-elasticsearch

# Create the repo for ElasticSearch
vim /etc/yum.repos.d/elk.repo

# Paste the next code snippet 
[elasticsearch-6.x]
name=Elasticsearch repository for 6.x packages
baseurl=https://artifacts.elastic.co/packages/6.x/yum
gpgcheck=1
gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
enabled=1
autorefresh=1
type=rpm-md

# Install ElasticSearch
yum install -y elasticsearch

# Install ingest-user-agent plugin
sudo /usr/share/elasticsearch/bin/elasticsearch-plugin install ingest-user-agent

# Install the geoip processor plugin.
sudo /usr/share/elasticsearch/bin/elasticsearch-plugin install ingest-geoip

# Start & Ensable the service 
systemctl daemon-reload
systemctl enable elasticsearch
systemctl start elasticsearch

# Edit the configuration file 
vim /etc/elasticsearch/elasticsearch.yml

# Uncomment that:
network.host: localhost

# Restart the service  
systemctl restart elasticsearch

# Make sure that ElasticSearch API is available 
curl -X GET http://localhost:9200

# Create the repo for Kibana
vim /etc/yum.repos.d/kibana.repo

# Paste the next code snippet
[kibana-4.4]
name=Kibana repository for 4.4.x packages
baseurl=http://packages.elastic.co/kibana/4.4/centos
gpgcheck=1
gpgkey=http://packages.elastic.co/GPG-KEY-elasticsearch
enabled=1

# Install Kibana 
sudo yum -y install kibana

# Edit the configuration file
vim /etc/kibana/kibana.yml

# Uncomment that:
server.host: "localhost"

# Start the server 
sudo systemctl start kibana
sudo chkconfig kibana on

# Install httpd-tools 
yum -y install httpd-tools

# Create admin user to use the webUI  
sudo htpasswd -c /etc/nginx/htpasswd.users kibanaadmin

# Edit the kibana configuration file 
vim /etc/nginx/conf.d/kibana.conf

# Paste the next code snippet 
server {
    listen 80;

    server_name congitev-task.com;

    auth_basic "Restricted Access";
    auth_basic_user_file /etc/nginx/htpasswd.users;

    location / {
        proxy_pass http://localhost:5601;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
}

# Restart the service 
sudo systemctl restart nginx

# Selinux Issue 
sudo setsebool -P httpd_can_network_connect 1

# Create logstash repo
sudo vim /etc/yum.repos.d/logstash.repo

# Add the next code snippet:
[logstash-2.2]
name=logstash repository for 2.2 packages
baseurl=http://packages.elasticsearch.org/logstash/2.2/centos
gpgcheck=1
gpgkey=http://packages.elasticsearch.org/GPG-KEY-elasticsearch
enabled=1

# Install logstash
yum -y install logstash

# Open the openssl configuration file
vim /etc/pki/tls/openssl.cnf

# Add this line:  
subjectAltName = IP: 192.169.1.2

# Generate the ssl certificate 
sudo openssl req -config /etc/pki/tls/openssl.cnf -x509 -days 3650 -batch -nodes -newkey rsa:2048 -keyout private/logstash-forwarder.key -out certs/logstash-forwarder.crt

# Create a configuration file for logstash 
vim /etc/logstash/conf.d/02-beats-input.conf

# Paste the next code snippet 
input {
  beats {
    port => 5044
    ssl => true
    ssl_certificate => "/etc/pki/tls/certs/logstash-forwarder.crt"
    ssl_key => "/etc/pki/tls/private/logstash-forwarder.key"
  }
}

# Create a configuration file for logstash
vim /etc/logstash/conf.d/10-syslog-filter.conf

# Paste the next code snippet  
filter {
  if [type] == "syslog" {
    grok {
      match => { "message" => "%{SYSLOGTIMESTAMP:syslog_timestamp} %{SYSLOGHOST:syslog_hostname} %{DATA:syslog_program}(?:\[%{POSINT:syslog_pid}\])?: %{GREEDYDATA:syslog_message}" }
      add_field => [ "received_at", "%{@timestamp}" ]
      add_field => [ "received_from", "%{host}" ]
    }
    syslog_pri { }
    date {
      match => [ "syslog_timestamp", "MMM  d HH:mm:ss", "MMM dd HH:mm:ss" ]
    }
  }
}

# Create a configuration file for logstash
vim /etc/logstash/conf.d/30-elasticsearch-output.conf

# Paste the next code snippet
output {
  elasticsearch {
    hosts => ["localhost:9200"]
    sniffing => true
    manage_template => false
    index => "%{[@metadata][beat]}-%{+YYYY.MM.dd}"
    document_type => "%{[@metadata][type]}"
  }
}

# Test your Logstash configuration 
service logstash configtest

# Restart the service and enable 
sudo systemctl restart logstash
sudo systemctl enable logstash
sudo chkconfig logstash on

# Configure the firewall
firewall-cmd --permanent --add-port=5044/tcp
firewall-cmd --permanent --add-port=5601/tcp
firewall-cmd --reload

# Download the Kibana dashboard archive locally and extract it 
cd ~
curl -L -O https://download.elastic.co/beats/dashboards/beats-dashboards-1.1.0.zip
unzip beats-dashboards-*.zip

# Load the dashboard 
cd beats-dashboards-*
./load.sh

# Download the Filebeat index template locally 
cd ~
curl -O https://gist.githubusercontent.com/thisismitch/3429023e8438cc25b86c/raw/d8c479e2a1adcea8b1fe86570e42abab0f10f364/filebeat-index-template.json

# Load the template
curl -XPUT 'http://localhost:9200/_template/filebeat?pretty' -d@filebeat-index-template.json

# Install metricbeat
sudo yum install metricbeat

# Create a temporary JSON file
vim template.json

# Insert the next code snippet:
"template": "*",
"settings":
"index":
"number_of_shards": 1,
"number_of_replicas": 0

# Use curl to create an index template 
curl -H'Content-Type: application/json' -XPUT http://localhost:9200/_template/defaults -d @template.json

# Create /etc/filebeat/filebeat.yml and add the following content
vim /etc/filebeat/filebeat.yml

# Add:
filebeat.config.modules:
    path: ${path.config}/modules.d/*.yml

setup.kibana:
    host: "localhost:5601"

output.elasticsearch:
    hosts: ["localhost:9200"]

setup.dashboards.enabled: true

# Enable Filebeat and enable the NGINX module:
sudo mv /etc/filebeat/modules.d/nginx.yml.disabled /etc/filebeat/modules.d/nginx.yml

# Enable and restart the service
sudo systemctl enable filebeat
sudo systemctl start filebeat

# Create the following file /etc/metricbeat/metricbeat.yml
vim /etc/metricbeat/metricbeat.yml

# Add:
metricbeat.config.modules:
  path: ${path.config}/modules.d/*.yml

setup.kibana:
  host: "localhost:5601"

output.elasticsearch:
  hosts: ["localhost:9200"]

setup.dashboards.enabled: true

# Rename the Elasticsearch, Kibana, and NGINX module configuration files to enable them
sudo mv /etc/metricbeat/modules.d/elasticsearch.yml.disabled /etc/metricbeat/modules.d/elasticsearch.yml
sudo mv /etc/metricbeat/modules.d/kibana.yml.disabled /etc/metricbeat/modules.d/kibana.yml
sudo mv /etc/metricbeat/modules.d/nginx.yml.disabled /etc/metricbeat/modules.d/nginx.yml

# Start and enable the service
sudo systemctl enable metricbeat
sudo systemctl start metricbeat

# Add the location block in /etc/nginx/nginx.conf file 
vim /etc/nginx/nginx.conf

# Add the following code snippet 
    location /server-status {
            stub_status on
            access_log off;
            allow 127.0.0.1;
            allow ::1;
            deny all;
        }

# Restart the service
sudo systemctl restart nginx
